from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium import webdriver
from .views import index, api
import time

# Create your tests here.
class Story9UnitTest(TestCase):
    def test_url_landing_exist(self):
        response = Client().get('/landing/')
        self.assertEqual(response.status_code, 200)

    def test_function_index(self):
        found = resolve('/landing/')
        self.assertEqual(found.func, index)

    def test_template(self):
        response = Client().get('/landing/')
        self.assertTemplateUsed(response, 'index.html')

    def test_url_data_is_exist(self):
        response = Client().get('/data/')
        self.assertEqual(response.status_code, 200)

    def test_function_data(self):
        found = resolve('/data/')
        self.assertEqual(found.func, api)
