from django.shortcuts import render
from django.http import JsonResponse
import urllib.request
import json

# Create your views here.

def api_input(request,param):
  url = 'https://www.googleapis.com/books/v1/volumes?q=' + param
  req = urllib.request.Request(url)
  r = urllib.request.urlopen(req).read()
  my_dict = json.loads(r.decode('utf-8'))
  return JsonResponse(my_dict)

def index(request):
  return render(request, "index.html")

def api(request):
  url = 'https://www.googleapis.com/books/v1/volumes?q=quilting'
  req = urllib.request.Request(url)
  r = urllib.request.urlopen(req).read()
  my_dict = json.loads(r.decode('utf-8'))
  return JsonResponse(my_dict)